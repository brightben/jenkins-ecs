output "efs_id" {
  description = "ID of EFS created"
  value       = aws_efs_file_system.this.id
}

output "efs_arn" {
  description = "ARN of EFS created"
  value       = aws_efs_file_system.this.arn
}

output "efs_dns_name" {
  description = "DNS name of EFS created"
  value       = aws_efs_file_system.this.dns_name
}

output "mount_target_ids" {
  description = "Map of created mount target IDs to subnet created in"
  value       = { for s, t in aws_efs_mount_target.this : s => t.id }
}

output "mount_target_dns_names" {
  description = "Map of DNS names of mount targets created to subnet created in"
  value       = { for s, t in aws_efs_mount_target.this : s => t.dns_name }
}

output "client_sg" {
  description = "ID of security group for clients that will mount EFS volume"
  value       = aws_security_group.client.id
}
