resource "aws_efs_file_system" "this" {
  creation_token = var.name
  tags           = merge({ Name = var.name }, var.tags)
  encrypted      = true

  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }
}

# Mount target resources
resource "aws_security_group" "mount_target" {
  name        = "${var.name}-mount-target"
  description = "For ${var.name} mount target"
  vpc_id      = var.vpc_id
  tags        = merge({ Name = "${var.name}-mount-target" }, var.tags)
}

# Must use the separate SG rule form because SGs must be created before rules to avoid
# circular dependency.
resource "aws_security_group_rule" "mount_target_ingress" {
  description              = "Allow NFS traffic into mount target from client SG"
  type                     = "ingress"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
  security_group_id        = aws_security_group.mount_target.id
  source_security_group_id = aws_security_group.client.id
}

resource "aws_efs_mount_target" "this" {
  for_each = var.subnets

  file_system_id  = aws_efs_file_system.this.id
  subnet_id       = each.key
  security_groups = [aws_security_group.mount_target.id]
}

# FS client resources
resource "aws_security_group" "client" {
  name        = "${var.name}-client"
  description = "For clients that mount ${var.name}"
  vpc_id      = var.vpc_id
  tags        = merge({ Name = "${var.name}-client" }, var.tags)

  depends_on = [aws_efs_mount_target.this]
}

resource "aws_security_group_rule" "client_egress" {
  description              = "Allow NFS traffic out from clients to mount target"
  type                     = "egress"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
  security_group_id        = aws_security_group.client.id
  source_security_group_id = aws_security_group.mount_target.id
}
