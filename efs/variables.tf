variable "name" {
  description = "Name for filesystem and associated resources"
  type        = string
}

variable "tags" {
  description = "Tags to apply to all resources"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "ID of VPC to create EFS mount targets within"
}

variable "subnets" {
  description = "Set of subnet IDs to create mount targets in"
  type        = set(string)
}
