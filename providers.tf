terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "jenkins-ecs-tfstate"
    key            = "infra.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-state-locking"
  }
}

provider "aws" {
  region = local.region
}
