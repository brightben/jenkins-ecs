variable "name" {
  type = string
}

resource "aws_ecr_repository" "this" {
  name = var.name

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "this" {
  repository = aws_ecr_repository.this.name
  policy = jsonencode(
    {
      "rules" : [
        {
          "action" : {
            "type" : "expire"
          },
          "selection" : {
            "countType" : "sinceImagePushed",
            "countUnit" : "days",
            "countNumber" : 7,
            "tagStatus" : "untagged"
          },
          "description" : "Expire untagged images",
          "rulePriority" : 1
        }
      ]
    }
  )
}

output "repository_url" {
  value = aws_ecr_repository.this.repository_url
}
