locals {
  jenkins_port           = 8080
  jenkins_container_name = "jenkins"
}

resource "aws_efs_access_point" "this" {
  file_system_id = var.fs_id

  posix_user {
    gid = 1000
    uid = 1000
  }

  root_directory {
    path = "/jenkins-master-home"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = 755
    }
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = var.name
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  cpu                      = 1024
  memory                   = 2048
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  container_definitions = jsonencode(
    [
      {
        "name"      = local.jenkins_container_name
        "image"     = "jenkins/jenkins:lts"
        "essential" = true
        "logConfiguration" = {
          "logDriver" = "awslogs"
          "options" = {
            "awslogs-region"        = var.region
            "awslogs-group"         = var.cw_log_group
            "awslogs-stream-prefix" = "jenkins"
          }
        }
        "mountPoints" = [
          {
            "sourceVolume"  = "jenkins-master-home"
            "containerPath" = "/var/jenkins_home"
          }
        ]
        "portMappings" = [
          {
            "containerPort" = local.jenkins_port
          }
        ]
      }
    ]
  )

  volume {
    name = "jenkins-master-home"

    efs_volume_configuration {
      file_system_id     = var.fs_id
      transit_encryption = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.this.id
        iam             = "ENABLED"
      }
    }
  }
}

resource "aws_service_discovery_service" "this" {
  name = var.name

  dns_config {
    namespace_id = var.service_discovery_namespace_id
    dns_records {
      ttl  = 10
      type = "A"
    }
    routing_policy = "MULTIVALUE"
  }
}

resource "aws_security_group" "task" {
  name   = "${var.name}-task-sg"
  vpc_id = var.vpc_id
  tags   = { Name = "${var.name}-task-sg" }
}

resource "aws_ecs_service" "this" {
  name             = var.name
  cluster          = var.cluster_id
  task_definition  = aws_ecs_task_definition.this.arn
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  // health_check_grace_period_seconds  = 300
  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent         = 100

  network_configuration {
    subnets          = var.task_subnets
    assign_public_ip = false
    security_groups = concat(
      [aws_security_group.task.id],
      var.task_security_groups
    )
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.this.arn
    container_name = var.name
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.this.arn
    container_name   = local.jenkins_container_name
    container_port   = local.jenkins_port
  }
}
