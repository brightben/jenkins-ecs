variable "name" {
  description = "Name of task and associated resources"
  type        = string
}

variable "region" {
  description = "Region being launched in"
  type        = string
}

variable "account_id" {
  description = "Account being deployed to"
  type        = string
}

variable "cw_log_group" {
  description = "CloudWatch log group to send logs to"
  type        = string
}

variable "execution_role_arn" {
  description = "ARN of IAM role for ECS to use when executing task"
  type        = string
}

variable "task_role_arn" {
  description = "ARN of IAM role to associate with Jenkins master task"
  type        = string
}

variable "fs_id" {
  description = "ID of master EFS volume"
  type        = string
}

variable "service_discovery_namespace_id" {
  description = "ID of namespace for service discovery"
  type        = string
}

variable "vpc_id" {
  description = "ID of VPC task will be deployed in"
  type        = string
}

variable "cluster_id" {
  description = "ID of ECS cluster to launch service in"
  type        = string
}

variable "lb_subnets" {
  description = "List of subnets to launch load balancer in"
  type        = list(string)
}

variable "task_subnets" {
  description = "List of subnets to launch service in"
  type        = list(string)
}

variable "task_security_groups" {
  description = "List of security groups to attach to task instances"
  type        = list(string)
}
