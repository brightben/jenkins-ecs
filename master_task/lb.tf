locals {
  lb_port     = 80
  lb_protocol = "HTTP"
}

resource "aws_lb_target_group" "this" {
  name                 = "${var.name}-tg"
  port                 = local.lb_port
  protocol             = local.lb_protocol
  vpc_id               = var.vpc_id
  target_type          = "ip"
  deregistration_delay = 10

  health_check {
    path = "/login"
  }
}

resource "aws_security_group" "lb" {
  name   = "${var.name}-lb-sg"
  vpc_id = var.vpc_id
  tags   = { Name = "${var.name}-lb-sg" }

  ingress {
    description = "Inbound from world"
    from_port   = local.lb_port
    to_port     = local.lb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description     = "Outbound to Jenkins UI"
    from_port       = local.jenkins_port
    to_port         = local.jenkins_port
    protocol        = "tcp"
    security_groups = [aws_security_group.task.id]
  }
}

resource "aws_security_group_rule" "task_lb_ingress" {
  description              = "Inbound from load balancer"
  type                     = "ingress"
  from_port                = local.jenkins_port
  to_port                  = local.jenkins_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.task.id
  source_security_group_id = aws_security_group.lb.id
}

resource "aws_lb" "this" {
  name               = "${var.name}-task-lb"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb.id]
  subnets            = var.lb_subnets
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = aws_lb.this.arn
  port              = local.lb_port
  protocol          = local.lb_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }
}
