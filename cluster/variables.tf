variable "name" {
  description = "Name for ECS cluster and associated resources"
  type        = string
}

variable "vpc_id" {
  description = "ID of VPC to launch namespace in"
  type        = string
}
