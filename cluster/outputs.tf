output "cluster_id" {
  description = "ID of ECS cluster"
  value       = aws_ecs_cluster.this.id
}

output "cluster_namespace_id" {
  description = "ID of private namespace for cluster"
  value       = aws_service_discovery_private_dns_namespace.this.id
}
