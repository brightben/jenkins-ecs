variable "name" {
  description = "Name to label all IAM resources with"
  type        = string
}

variable "region" {
  description = "Region resources are deployed to"
  type        = string
}

variable "account_id" {
  description = "ID of account being deployed to"
  type        = string
}
