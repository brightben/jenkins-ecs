#
# IAM resources kept separate because they require ADMIN privs to edit
#

# General ECS task resources
resource "aws_iam_role" "task_execution" {
  name        = "${var.name}_taskExecution"
  description = "Role assumed by AWS ECS service when executing tasks for ${var.name} cluster"

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "ecs-tasks.amazonaws.com"
          },
          "Action" : "sts:AssumeRole"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "task_execution_aws_standard" {
  role       = aws_iam_role.task_execution.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_policy" "task_instance_base" {
  name        = "${var.name}_taskInstanceBase"
  description = "Provides access to CloudWatch logging"

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "CreateLogStream",
          "Effect" : "Allow",
          "Action" : "logs:CreateLogStream",
          "Resource" : "arn:aws:logs:${var.region}:${var.account_id}:log-group:${var.name}*"
        },
        {
          "Sid" : "PutLogEvents",
          "Effect" : "Allow",
          "Action" : "logs:PutLogEvents",
          "Resource" : "arn:aws:logs:${var.region}:${var.account_id}:log-group:${var.name}*:log-stream:*"
        }
      ]
    }
  )
}

# Jenkins master task
resource "aws_iam_role" "jenkins_master_task" {
  name        = "${var.name}_jenkinsMasterTask"
  description = "Role assumed by Jenkins master ECS task instance for accessing AWS services"

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "ecs-tasks.amazonaws.com"
          },
          "Action" : "sts:AssumeRole"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "base_policy" {
  role       = aws_iam_role.jenkins_master_task.name
  policy_arn = aws_iam_policy.task_instance_base.arn
}

resource "aws_iam_role_policy" "jenkins_master_task_inline" {
  name = "JenkinsMaster"
  role = aws_iam_role.jenkins_master_task.name

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "JenkinsFSAccess",
          "Effect" : "Allow",
          "Action" : "elasticfilesystem:Client*",
          "Resource" : "arn:aws:elasticfilesystem:${var.region}:${var.account_id}:file-system/*",
          "Condition" : {
            "StringLike" : {
              "aws:ResourceTag/Name" : "${var.name}*"
            }
          }
        },
        {
          "Sid": "ListSecrets",
          "Effect": "Allow",
          "Action" : "secretsmanager:ListSecrets",
          "Resource" : "*"
        },
        {
          "Sid": "GetSecrets",
          "Effect": "Allow",
          "Action" : "secretsmanager:GetSecretValue",
          "Resource" : "arn:aws:secretsmanager:${var.region}:${var.account_id}:secret:${var.name}*"
        }
      ]
    }
  )
}
