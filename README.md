# Jenkins in AWS ECS Fargate

## Deploy

1. `terraform init`
1. Some resources depend on outputs from the VPC module before they can be successfully planned, so create the VPC individually first:

        terraform plan -out tfplan -target module.vpc
        terraform apply tfplan

1. Continue with remaining resources

        terraform plan -out tfplan
        terraform apply tfplan

## Destroy

Tear the whole stack down:

    terraform destroy

Tear down only the most expensive resources:

    terraform destroy -target module.vpc -target module.master_fs

## References

* https://tomgregory.com/deploy-jenkins-into-aws-ecs/
* https://aws.amazon.com/blogs/aws/amazon-ecs-supports-efs/
