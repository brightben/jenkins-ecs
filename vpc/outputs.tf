output "vpc_id" {
  value = module.awsvpc.vpc_id
}

output "private_subnets" {
  value = module.awsvpc.private_subnets
}

output "public_subnets" {
  value = module.awsvpc.public_subnets
}

output "primary_sg" {
  description = "ID of security group"
  value       = aws_security_group.this.id
}
