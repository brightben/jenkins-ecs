# Repository for master node images
module "master_repo" {
  source = "./ecr_repo"

  name = local.name
}

# Filesystem for master node
module "master_fs" {
  source = "./efs"

  name    = "${local.name}-master-fs"
  vpc_id  = module.vpc.vpc_id
  subnets = toset(module.vpc.private_subnets)
}

# Jenkins master task
module "master_task" {
  source = "./master_task"

  name                           = "${local.name}-master"
  region                         = local.region
  account_id                     = local.account_id
  cw_log_group                   = local.name
  execution_role_arn             = module.iam.task_execution_role_arn
  task_role_arn                  = module.iam.jenkins_master_task_role_arn
  fs_id                          = module.master_fs.efs_id
  service_discovery_namespace_id = module.cluster.cluster_namespace_id
  vpc_id                         = module.vpc.vpc_id
  cluster_id                     = module.cluster.cluster_id
  lb_subnets                     = module.vpc.public_subnets
  task_subnets                   = module.vpc.private_subnets
  task_security_groups = [
    module.vpc.primary_sg,
    module.master_fs.client_sg
  ]
}
