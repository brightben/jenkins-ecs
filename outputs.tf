output "master_repo_url" {
  value = module.master_repo.repository_url
}

output "master_lb_dns" {
  value = module.master_task.lb_dns_name
}
